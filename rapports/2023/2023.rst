.. index::
   pair: Rapport ; CNCDH (2022)

.. _rapport_cncdh_2023:

======================================================================
**Rapport CNCDH 2023**
======================================================================


Rapport 2023 sur la lutte contre le racisme, l'antisémitisme et la xénophobie
=======================================================================================

- https://www.cncdh.fr/sites/default/files/2024-06/CNCDH_Rapport_Racisme_2023.pdf


Les essentiels rapport 2023 sur la lutte contre le racisme, l’antisémitisme et la xénophobie
======================================================================================================

- https://www.cncdh.fr/sites/default/files/2024-06/CNCDH_Les_Essentiels_Rapport_Racisme_2023_0.pdf

Erratum : parmi tous les chiffres figurant dans Les Essentiels diffusés 
le jeudi 27 juin 2024, une erreur s’est glissée sur le chiffre en page 12 
concernant l’évolution de l'opinion des Français sur l'affirmation 
"Les enfants d’immigrés nés en France ne sont pas vraiment français". 

le résultat est de 23% et non de 43 %. 

Nous vous présentons nos plus vive excuses pour cette erreur.

Le communiqué de presse: Lutte contre le racisme, l’antisémitisme et la xénophobie : les actes racistes explosent, la tolérance recule
============================================================================================================================================

- https://www.cncdh.fr/sites/default/files/2024-06/27.06.24.%20CP%20Rapport%20Racisme%202023.pdf

La faible mobilisation du Gouvernement 79 % des personnes interrogées estiment qu’"une
lutte vigoureuse contre l’antisémitisme est nécessaire en France.".

"Il est regrettable que le Gouvernement n’ait pas la même préoccupation, déplore Jean-Marie
Burguburu, président de la CNCDH. 
Il s’est montré trop attentiste alors que les actes antisémites explosaient 
dans le pays à la suite du 7 octobre. 

Il aurait dû mobiliser tout de suite l’appareil de l’Etat, en accélérant la mise en
œuvre du Plan national de lutte contre le racisme, l’antisémitisme et les discriminations
adopté par Elisabeth Borne début 2023."

Analyse
=============

L’état du racisme en France en 2023

Le nombre de faits recensés à caractère raciste, antisémites et xénophobes 
se maintient à un niveau élevé. 

Ce sont plus de 8 500 crimes et délits à caractère raciste, xénophobe ou 
anti-religieux qui ont été enregistrés en 2023. 

1 676 actes antisémites ont été comptabilisés, ce qui représente une  hausse inédite et un niveau sans précédent
-------------------------------------------------------------------------------------------------------------------

1 676 actes antisémites ont été comptabilisés, ce qui représente une 
hausse inédite et un niveau sans précédent.

Le racisme en France reste encore largement sous-estimé et sous-déclaré. 

Il se manifeste souvent à travers des formes de rejet détournées, parfois 
difficiles à caractériser et à dénoncer par les victimes.

Cette année, la CNCDH a choisi de consacrer le focus aux discriminations 
liées à l’origine dans le monde du travail.
